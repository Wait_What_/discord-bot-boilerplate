# Database
This bot uses enmap. You can find better usage guides on google

# Usage guide
```js
const { db } = require('../bot')
// The db object contains 3 enmaps: user, guild and bot

// Set a value
db.user.setD('someUserID', { hi: 'hello' })

// Get the value
const value = db.user.getD('someUserID')
console.log(value.hi) // => 'hello'
```
The database won't disappear after restarting the bot, unless you remove the name property from the constructor

# Defaults
**Use `getD` and `setD`** if using the guild or user databases. This ensures that the default object specified in `config.js` exists in that key.
```js
const { db } = require('../bot)

// Using setD instead of set
db.user.setD(id, 420, 'balance')

// getD will return the default object instead of creating a useless entry in the database if the key doesn't exist yet
db.guild.getD(id, 'path')
```

# Enmap docs
For more info, check out [enmap.evie.dev](https://enmap.evie.dev/)