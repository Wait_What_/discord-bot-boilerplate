# Commands
## Dev
Command | Description | Usage | Aliases | DMs | Permissions
 ------ | ----------- | ----- | ------- | --- | ---------- 
`asynceval` | Run code asynchronously  | `asynceval await someCode()` | `ae` | No | `Bot owner`
`blacklist` | Blacklist a user |   |   | No | `Bot owner`
`eval` | Run code | `eval someCode()` | `e` | No | `Bot owner`
`reload` | Reload the commands without restarting the bot | `reload [silent]` | `rel`, `r` | No | `Bot owner`
`silenteval` | Run code silently | `silenteval someCode()` | `se` | No | `Bot owner`
`stop` | Stop the bot |   | `kys` | No | `Bot owner`

## Misc
Command | Description | Usage | Aliases | DMs | Permissions
 ------ | ----------- | ----- | ------- | --- | ---------- 
`help` | See all commands and their usage |   |   | Yes | -
`ping` | See the bot's ping |   | `pong` | Yes | -
`prefix` | Change the prefix in this server | `prefix [prefix]` | `setprefix` | No | `Manage messages`