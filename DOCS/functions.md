# Functions 
The bot has some functions that do very useful things. They are meant to save time and make code
more readable.

You can access them from the global `dbb` object (discord bot boilerplate)

- `dbb.embed([color])`
    - Returns a RichEmbed with the default color so you wouldn't have to require `discord.js` or `config`
- `dbb.sleep(ms)`
    - Returns a Promise that's resolved in a specified amount of miliseconds
    - Usage: `await sleep(500)`
- `dbb.guildCount()` / `dbb.userCount()`
    - Returns a Promise with the total guild/user count from all shards
- `dbb.getPrefix(id || null)`
    - Gets the prefix in a guild

## Example usage
```js
// Imagine that this is the exports.run function of some command
const embed = dbb.embed()
    .setImage('https://url.com/image.nice')

m.channel.send({ embed })
```

```js
const foo = async () => {
    console.log('hi')
    await dbb.sleep(2 * 1000) // Will wait for 2 seconds
    console.log('hello')
}
foo()
```