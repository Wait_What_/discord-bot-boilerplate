# Mods
`discord-bot-boilerplate` adds extra features to `enmap` and `discord.js`

```js
const Discord = require('discord.js')

const embed = new Discord.Embed(color)
// Returns a RichEmbed with the default color or the one provided in the constructor

Message.respond(content, title)
// Sends an embed with the description as the first argument and an author as the second (optional)
```

```js
const { db } = require('../../bot')

// Use getD and setD instead of set and get (when using the guild or user databases)
// This ensures that all DB entries have the default values specified in config.js
db.guild.getD('key')
db.guild.setD('key')
```