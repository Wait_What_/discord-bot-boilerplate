const { bot, config, db } = require('../../bot')

exports.run = () => {
    // Log in to Discord
    console.log('Logging in\n')
    bot.login(config.tokens.discord)

    // When the bot is logged in
    bot.on('ready', async () => {
        // Log a cool looking message
        const lines = [
            `${config.name} is online`,
            `${await dbb.guildCount()} guilds, ${await dbb.userCount()} users` +
                (bot.shard ? `, ${bot.shard.count} shard${bot.shard.count > 1 ? 's' : ''}` : ''),
            `Owner${config.owners.length > 1 ? 's:' : ' -'} ` +
                `${config.owners.map(id => bot.users.get(id).tag).join(', ')}`,
            `Prefix: ${config.defaultPrefix}`,
            `DB stats: ${db.user.size} user${db.user.size > 1 ? 's' : ''}, ` +
                `${db.guild.size} guild${db.guild.size > 1 ? 's' : ''}`
        ]

        const lineLength = Math.max(...lines.map(line => line.length))

        console.log([
            '#'.repeat(lineLength + 4),
            lines.map(line => `# ${line}${' '.repeat(lineLength - line.length)} #`).join('\n'),
            '#'.repeat(lineLength + 4)
        ].join('\n'))

        // Set the bot's activity
        const activity = async () => config.activity.text
            .replace(/BOTservers/g, await dbb.guildCount())
            .replace(/BOTusers/g, await dbb.userCount())
            .replace(/BOTprefix/g, config.defaultPrefix)

        bot.user.setActivity(await activity(), config.activity)
        setInterval(async () => {
            bot.user.setActivity(await activity(), config.activity)
        }, 60 * 1000)
    })

    // Ignore all errors from Discord
    bot.on('error', () => {})
}

exports.meta = {
    name: 'login',
    autorun: 3
}