// Load and mod enmap, load discord.js, load config
const [ Enmap, Discord, config ] = [
    require('./library/enmapMod'), require('discord.js'), require('./config')
]

// Start loading the bot
console.log(`Starting ${config.name}`)

// Create databases
const db = {
    bot: new Enmap({ name: 'botdata' }),
    guild: new Enmap({ name: 'guilddata', defaults: config.dbDefaults.guild }),
    user: new Enmap({ name: 'userdata', defaults: config.dbDefaults.user })
}

// Create a Discord client
const bot = new Discord.Client()

// Create a collection for modules
const modules = new Discord.Collection()

// Export client, databases, modules and config
module.exports = { bot, db, modules, config }

// Mod discord.js
require('./library/discordjsMod')(Discord, bot)

// Load functions
require('./library/functions')

// Load modules
const getFileList = require('./library/getFileList')
getFileList('./modules')
    .forEach(moduleName => {
        // Load module
        const module = require(moduleName)
        // Add to modules map
        modules.set(module.meta.name, module)
    })

// Run modules in order
modules
    .filter(module => module.meta.autorun)
    .sort((a, b) => a.meta.autorun - b.meta.autorun)
    .forEach(module => module.run())