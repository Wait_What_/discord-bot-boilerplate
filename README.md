# Discord bot boilerplate
[![MIT](https://flat.badgen.net/badge/License/MIT/blue)](https://gitlab.com/Waut_What_/antiowo/blob/master/LICENSE.md)
[![NODE](https://flat.badgen.net/badge/Language/Node.js/green?icon=node)](https://nodejs.org/en/)
[![SUPPORTSERVER](https://flat.badgen.net/badge/Support%20server/Join/purple)](https://discord.gg/N8Fqcuk)

A boilerplate for making discord bots easily using `discord.js` and `enmap`

Included features:
- Commands and modules
- Database (enmap)
- Example commands
- Command list generator
- Custom prefixes
- Basic sharding support
- Documentation
- And more!

## Where do I start?
Read [Usage](DOCS/usage.md) for an **install and usage guide**

Check out [DOCS](DOCS/) for full documentation

## Support
Discord [support server](https://discord.gg/N8Fqcuk)

Add me: `Wait What#4975`

## License
This project is licensed under [MIT](./LICENSE.md). Please consider including the following in your README:

[![DISCORDBOTBOILERPLATE](https://flat.badgen.net/badge/Made%20using/discord-bot-boilerplate/blue)](https://gitlab.com/BarkingDog/discord-bot-boilerplate)

```md
[![DISCORDBOTBOILERPLATE](https://flat.badgen.net/badge/Made%20using/discord-bot-boilerplate/blue)](https://gitlab.com/Wait_What_/discord-bot-boilerplate)
```