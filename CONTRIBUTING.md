# Contributing
Feel free to make a PR or create an issue.

## Code style/rules
These rules exist to keep the code consistent and to annoy anyone who's code style is different

- No semicolons anywhere
- 4 spaces for indentation
- Never use var
- Don't add useless features
- Use a mix of ES6 and also not ES6 in a way that'd make it fit in with the rest of the code