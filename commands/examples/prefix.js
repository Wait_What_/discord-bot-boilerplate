const { db, config } = require('../../bot')

exports.run = (m, a) => {
    // Get the prefix
    const prefix = a.join(' ')

    // If the prefix is not empty or not the default
    if (prefix && prefix != config.defaultPrefix) {
        // Set the prefix
        db.guild.setD(m.guild.id, prefix, 'prefix')
        // Send a message
        m.respond(`:white_check_mark: Set the prefix to \`${prefix}\``)
    } else {
        // Remove the prefix
        db.guild.setD(m.guild.id, undefined, 'prefix')
        // Send a message
        m.respond(`:white_check_mark: Reset the prefix to \`${config.defaultPrefix}\``)
    }
}

exports.meta = {
    names: [ 'prefix', 'setprefix' ],
    permissions: [ 'MANAGE_MESSAGES' ],
    help: {
        description: 'Change the prefix in this server',
        usage: '[prefix]',
        category: 'misc'
    }
}