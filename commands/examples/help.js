const { bot, modules } = require('../../bot')
const { help } = modules.get('commandLoader')

exports.run = (m, a) => {
    // Get the prefix and category
    const prefix = dbb.getPrefix(m.guild ? m.guild.id : null)
    const category = (a[0] || '').toLowerCase()

    // Create an embed
    const embed = dbb.embed()

    // If a valid category is provided
    if (help.has(category)) embed
        .setAuthor(`Help - ${category}`, bot.user.displayAvatarURL)
        .setDescription( // Add the list to the embed
            help.get(category).map(item =>
                `${item.permissions.length > 0 ? `:small_${item.permissions.includes('DM') ?
                'blue' : 'orange'}_diamond:` : ':white_small_square:'} \`${prefix}` +
                `${item.names[0]}\` ${item.description}${item.usage != '' ?
                ` \`${prefix}${item.names[0]} ${item.usage}\`` : ''}`
            ).join('\n')
        )
        .setFooter('Blue - supports DMs | orange - needs permissions')
    
    // Otherwise, send the list of categories
    else {
        // Generate list of categories
        const list = Array.from(help.keys())
            .map(category => `:white_small_square: \`${prefix}help ${category}\``)

        // Add the list to the embed
        embed
            .setAuthor('Help', bot.user.displayAvatarURL)
            .setDescription(`To see commands use:\n${list.join('\n')}`)
    }

    // Send the embeds
    m.channel.send({embed})
}

exports.meta = {
    names: [ 'help' ],
    permissions: [ 'DM' ],
    help: {
        description: 'See all commands and their usage',
        usage: '',
        category: 'misc'
    }
}