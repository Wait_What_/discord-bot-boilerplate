const { bot } = require('../../bot')

exports.run = (m, a) => {
    // Send an embed with the ping rounded up
    m.respond(`Pong! **${Math.ceil(bot.ping)}** ms :ping_pong:`)
}

exports.meta = {
    names: [ 'ping', 'pong' ],
    permissions: [ 'DM' ],
    help: {
        description: 'See the bot\'s ping',
        usage: '',
        category: 'misc'
    }
}