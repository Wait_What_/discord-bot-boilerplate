const { bot, config } = require('../../bot')

exports.run = async (m, a) => {
    // Send a message if a 'silent' argument is not provided
    if (!(a[0] || '').match(/^(-)*s/i)) await m.respond(`Stopping **${config.name}**...`)
    console.log(`Stopping ${config.name}`)

    // Log out
    await bot.destroy()
    // End the process
    process.exit()
}

exports.meta = {
    names: [ 'stop', 'kys', 'die' ],
    permissions: [ 'BOT_OWNER' ],
    help: {
        description: 'Stop the bot',
        usage: '',
        category: 'dev'
    }
}