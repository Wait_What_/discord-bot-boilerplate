const { bot, modules, db, config } = require('../../bot')

exports.run = (m, a) => {
    // Don't run the code if no arguments are provided
    if (a.length < 0) return

    try {
        // Run the code
        eval(a.join(' '))
    } catch (e) {
        // Log the error if it happens
        console.error(e)
    }
}

exports.meta = {
    names: [ 'silenteval', 'se' ],
    permissions: [ 'BOT_OWNER' ],
    help: {
        description: 'Run code silently',
        usage: 'someCode()',
        category: 'dev'
    }
}