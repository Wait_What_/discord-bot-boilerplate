const { bot, db } = require('../../bot')

exports.run = (m, a) => {
    // Get a user object
    const user = m.mentions.users.first() || bot.users.get(a[0])
    if (!user || user.id == m.author.id) return m.respond(':x: Provide a valid user')

    // Get the current status from the DB
    const current = db.user.getD(user.id, 'blacklist')

    // Send a message
    m.respond(`:white_check_mark: ${current ? 'Unb' : 'B'}lacklisted **${user.tag}**`)

    // Toggle the status in the DB
    db.user.set(user.id, !current, 'blacklist')
}

exports.meta = {
    names: [ 'blacklist', 'bl' ],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Blacklist a user',
        usage: '',
        category: 'dev'
    }
}