const { bot, modules, db, config } = require('../../bot')

exports.run = (m, a) => {
    // Load the inspector
    const inspect = require('util').inspect

    // If there are no arguments
    if (a.length < 1)  return // Don't run the code

    try {
        // Run the code and inspect the output
        const output = inspect(eval(a.join(' '))).replace(/`/g, '\`').slice(0, 1980)
        // Send the output in a message
        m.channel.send(`\`\`\`js\n${output}\n\`\`\``)
    } catch (e) {
        // Output any errors
        m.channel.send(`**\`ERROR\`**\n\`\`\`js\n${e}\n\`\`\``)
    }
}

exports.meta = {
    names: [ 'eval', 'e' ],
    permissions: [ 'BOT_OWNER' ],
    help: {
        description: 'Run code',
        usage: 'someCode()',
        category: 'dev'
    }
}