const { modules } = require('../../bot')

exports.run = async (m, a) => {
    // Send a message if a 'silent' argument is not provided
    if (!(a[0] || '').match(/^(-)*s/i)) await m.respond('Reloading commands')

    // Get the command loader module and run it
    console.log('Reloading commands')
    modules.get('commandLoader').run()
}

exports.meta = {
    names: [ 'reload', 'rel', 'r' ],
    permissions: [ 'BOT_OWNER' ],
    help: {
        description: 'Reload the commands without restarting the bot',
        usage: '[silent]',
        category: 'dev'
    }
}