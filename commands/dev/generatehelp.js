const { help } = require('../../modules/core/commandLoader')

exports.run = (m, a) => {
    // Function to properly capitalise strings
    const capitalise = str => (str || '')[0].toUpperCase() + (str || '').slice(1).toLowerCase()

    // Create an array of categories
    const categories = Array.from(help.keys())
        .map(category => [
                // Set a title for each category
                `## ${capitalise(category)}`,

                // Create the first line with column names
                'Command | Description | Usage | Aliases | DMs | Permissions',
                ' ------ | ----------- | ----- | ------- | --- | ---------- ',

                // Add a line for every command in the category
                help.get(category)
                    .map(cmd => [
                            // Add the command name
                            `\`${cmd.names[0]}\``,
                            // Add the description
                            cmd.description || '_No description._',
                            // Add the usage if it is provided
                            cmd.usage ? `\`${cmd.names[0]} ${cmd.usage}\`` : ' ',
                            // Add all aliases if they exist
                            cmd.names.slice(1).map(name => `\`${name}\``).join(', ') || ' ',
                            // Say if the command is supported in DMs
                            cmd.permissions.includes('DM') ? 'Yes' : 'No',
                            // Add formatted permissions required for this command
                            cmd.permissions
                                .filter(perm => perm != 'DM')
                                .map(name => `\`${capitalise(name.replace(/_/g, ' '))}\``)
                                .join(', ') || '-'
                        ].join(' | ') // Do some joining
                    ).join('\n') // More joining
            ].join('\n') // And more joining
        )

    // Log the table to the console
    console.log(`# Commands\n${categories.join('\n\n')}`)

    // Tell the user to look into the console
    m.respond(':white_check_mark: Table generated. Check the logs.')
}

exports.meta = {
    names: [ 'gentable' ],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Generates command list in markdown',
        usage: '',
        category: ''
    }
}