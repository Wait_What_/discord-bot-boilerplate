const config = require('../config')

module.exports = (Discord, bot) => {
    // Discord mods
    Discord.Embed = class extends Discord.RichEmbed {
        constructor(color, data) {
            super(data)
            this.setColor(color || config.embedColor)
        }
    }

    Discord.Message.prototype.respond = function(content, title) {
        if (!content || typeof content != 'string' || content.length < 1 || content.length > 2048)
            throw new Error('content must be a string from 1 to 2048 characters', 'DBBError')

        if (title && (typeof title != 'string' || title.length < 1 || title.length > 256))
            throw new Error('title must be undefined or a string from 1 to 256 characters', 'DBBError')

        const embed = new Discord.Embed()
            .setDescription(content)

        title && embed.setAuthor(title, bot.user.displayAvatarURL)

        return this.channel.send({embed})
    }
}