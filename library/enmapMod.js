const Enmap = require('enmap')

// Enmap mods
module.exports = class extends Enmap {
    constructor(options) {
        if (options.defaults) options.ensureProps = true
        super(options)

        if (options.defaults) this.defaults = options.defaults
    }

    getD(key, path) {
        if (!this.defaults) return super.get(key, path)
        if (!super.has(key)) return path ? this.defaults[path] : this.defaults

        super.ensure(key, this.defaults)
        return super.get(key, path)
    }

    setD(key, val, path) {
        this.defaults && super.ensure(key, this.defaults)
        return super.set(key, val, path)
    }

    push(key, val, path, allowDupes) {
        this.defaults && super.ensure(key, this.defaults)
        return super.push(key, val, path, allowDupes)
    }

    math(key, operation, operand, path) {
        this.defaults && super.ensure(key, this.defaults)
        return super.math(key, operation, operand, path)
    }

    inc(key, path) {
        this.defaults && super.ensure(key, this.defaults)
        return super.inc(key, path)
    }

    dec(key, path) {
        this.defaults && super.ensure(key, this.defaults)
        return super.dec(key, path)
    }

    remove(key, val, path) {
        this.defaults && super.ensure(key, this.defaults)
        return super.remove(key, val, path)
    }

    setProp() {
        throw new Error('setProp() is deprecated, use set()', 'DBBError')
    }

    getProp() {
        throw new Error('getProp() is deprecated, use get()', 'DBBError')
    }

    hasProp() {
        throw new Error('hasProp() is deprecated, use has()', 'DBBError')
    }

    pushIn() {
        throw new Error('pushIn() is deprecated, use push()', 'DBBError')
    }

    deleteProp() {
        throw new Error('deleteProp() is deprecated, use delete()', 'DBBError')
    }

    removeFrom() {
        throw new Error('removeFrom() is deprecated, use remove()', 'DBBError')
    }
}